package crud;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;


public class Service<T>{
	
	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("test");
	private static final EntityManager em = emf.createEntityManager();
	private Class<T> clazz;
	
	public Service(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		Query query = em.createQuery("select t from " + clazz.getCanonicalName() + " t");
		return (List<T>)query.getResultList();
	}
	
	public void dodaj(T t) {
		em.getTransaction().begin();
		try {
			em.persist(t);
			em.flush();
			em.getTransaction().commit();
		} catch(PersistenceException e) {
			em.getTransaction().rollback();
			throw new PersistenceException();
		}
	}
	
	public void usun(T t) {
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
	}
	
	public static void close() {
		em.close();
		emf.close();
	}
}
