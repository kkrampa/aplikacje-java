package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Klub {
	@Id
	@GeneratedValue
	private long id;
	
	private String nazwa;
	
	private String miejscowosc;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_trener")
	private Trener trener;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_stadion")
	private Stadion stadion;
	
    @OneToMany(mappedBy="id")
	private List<Pilkarz> pilkarze;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public Trener getTrener() {
		return trener;
	}

	public void setTrener(Trener trener) {
		this.trener = trener;
	}

	public Stadion getStadion() {
		return stadion;
	}

	public void setStadion(Stadion stadion) {
		this.stadion = stadion;
	}

	public void setPilkarze(List<Pilkarz> pilkarze) {
		this.pilkarze = pilkarze;
	}

	public List<Pilkarz> getPilkarze() {
		return pilkarze;
	}
	
	public String toString() {
		return nazwa + " " + miejscowosc;
	}
	
	
	
}
