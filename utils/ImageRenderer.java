package utils;

import java.awt.Component;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ImageRenderer extends DefaultTableCellRenderer {
	 
	private static final long serialVersionUID = -6985716190404383771L;

	@Override
    public Component getTableCellRendererComponent(JTable table,Object value, boolean isSelected,boolean hasFocus, int row, int column) {
        JLabel label = new JLabel();
 
        if (value != null) {
	       // label.setHorizontalAlignment(JLabel.CENTER);
        	ImageIcon ii = new ImageIcon((byte[])value);
        	int width = table.getColumnModel().getColumn(column).getWidth();
	        label.setIcon(new ImageIcon(ii.getImage().getScaledInstance(width, 200, Image.SCALE_SMOOTH)));
        }
 
        return label;
    }
}
