package controller;

import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import model.Klub;
import model.Pilkarz;
import model.Stadion;
import model.Trener;
import crud.Service;
import utils.ImageRenderer;
import view.Fetchable;
import view.Objectable;
import view.View;
import view.forms.KlubForm;
import view.forms.PilkarzForm;
import view.forms.StadionForm;
import view.forms.TrenerForm;
import view.models.KlubTableModel;
import view.models.PilkarzTableModel;
import view.models.StadionTableModel;
import view.models.TrenerTableModel;

public class Controller {
	
	
	private Service<Trener> trenerService = new Service<Trener>(Trener.class);
	private Service<Stadion> stadionService = new Service<Stadion>(Stadion.class);
	private Service<Klub> klubService = new Service<Klub>(Klub.class);
	private Service<Pilkarz> pilkarzService = new Service<Pilkarz>(Pilkarz.class);
	
	private TrenerTableModel tm = new TrenerTableModel();
	private StadionTableModel sm = new StadionTableModel();
	private KlubTableModel km = new KlubTableModel();
	private PilkarzTableModel pm = new PilkarzTableModel(); 
	
	private View view;
	
	private static int OPCJA = 0;
	
	TrenerForm trenerForm = new TrenerForm();
	StadionForm stadionForm = new StadionForm();
	KlubForm klubForm = new KlubForm(stadionService.getAll(), trenerService.getAll());
	PilkarzForm pilkarzForm = new PilkarzForm(klubService.getAll());
	
	ActionListener usunListener = new UsunListener<Trener>(trenerService, tm);
	List<String[]> lista = new ArrayList<String[]>();

	public Controller(View view) {
		this.view = view;
		
		tm.addTableModelListener(new AktualizujListener<Trener>(trenerService, tm));
		sm.addTableModelListener(new AktualizujListener<Stadion>(stadionService, sm));
		km.addTableModelListener(new AktualizujListener<Klub>(klubService, km));
		pm.addTableModelListener(new AktualizujListener<Pilkarz>(pilkarzService, pm));
		
		stadionForm.addZapiszListener(new DodajListener<Stadion>(stadionForm, stadionService, sm, stadionForm));
		trenerForm.addZapiszListener(new DodajListener<Trener>(trenerForm, trenerService, tm, trenerForm));
		klubForm.addZapiszListener(new DodajListener<Klub>(klubForm, klubService, km, klubForm));
		pilkarzForm.addZapiszListener(new DodajListener<Pilkarz>(pilkarzForm, pilkarzService, pm, pilkarzForm));
		
		tm.setObjects(trenerService.getAll());
		
		view.addWindowListener(new WindowAdapter() {
			 public void windowClosing(WindowEvent e) {
				//trenerService.close();
				//stadionService.close();
				 Service.close();
			 }
			
		});
		view.addDodajListener(new FormListener());
		view.getJtable().setModel(tm);
		view.addTrenerListener(new AddTrenerListener());
		view.addStadionListener(new AddStadionListener());
		view.addklubListener(new AddKlubListener());
		view.addPilkarzListener(new AddPilkarzListener());
		view.addUsunListener(usunListener);
		view.setVisible(true);
		
		view.getJtable().addMouseListener(new MouseAdapter() {
			 @Override
			 public void mouseClicked(java.awt.event.MouseEvent evt) {
				 if(OPCJA == 3) {
					 int row = Controller.this.view.getJtable().rowAtPoint(evt.getPoint());
				     int col = Controller.this.view.getJtable().columnAtPoint(evt.getPoint());
				     if(col == 3) {
				    	 JFileChooser fc = new JFileChooser();
				    	 int result = fc.showOpenDialog(Controller.this.view);
				    	 if (result == JFileChooser.APPROVE_OPTION) {
				    		 File file = fc.getSelectedFile();
				            	FileInputStream fileInputStream = null;
				                byte[] bFile = new byte[(int) file.length()];

				            	try {
									fileInputStream = new FileInputStream(file);
									fileInputStream.read(bFile);
					        	    fileInputStream.close();
						    		Controller.this.view.getJtable().getModel().setValueAt(bFile, row, col);

								} catch (FileNotFoundException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (IOException e2) {
									// TODO Auto-generated catch block
									e2.printStackTrace();
								}
				    	 }
				     }
				 }
			 }
		});
	}
	
	class AddTrenerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			OPCJA = 0;
			tm.setObjects(trenerService.getAll());
			view.removeUsunListener(usunListener);
			usunListener = new UsunListener<Trener>(trenerService, tm);
			view.addUsunListener(usunListener);
			view.getJtable().setRowHeight(20);
			view.getJtable().setModel(tm);
		}
		
	}
	
	class AddStadionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			OPCJA = 1;
			sm.setObjects(stadionService.getAll());
			view.removeUsunListener(usunListener);
			usunListener = new UsunListener<Stadion>(stadionService, sm);
			view.addUsunListener(usunListener);
			view.getJtable().setRowHeight(20);
			view.getJtable().setModel(sm);
		}
		
	}
	
	class AddKlubListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			OPCJA = 2;
			km.setObjects(klubService.getAll());
			view.removeUsunListener(usunListener);
			usunListener = new UsunListener<Klub>(klubService, km);
			view.addUsunListener(usunListener);
			view.getJtable().setModel(km);
			view.getJtable().setRowHeight(20);
			new ComboBoxColumn<Trener>(3, trenerService);
			new ComboBoxColumn<Stadion>(2, stadionService);
		}
		
	}
	
	class AddPilkarzListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			OPCJA = 3;
			pm.setObjects(pilkarzService.getAll());
			view.removeUsunListener(usunListener);
			usunListener = new UsunListener<Pilkarz>(pilkarzService, pm);
			view.addUsunListener(usunListener);
			view.getJtable().setModel(pm);
			view.getJtable().setRowHeight(200);
			new ComboBoxColumn<Klub>(2, klubService);
			view.getJtable().getColumnModel().getColumn(3).setCellRenderer(new ImageRenderer());
		}
		
	}
	
	class AktualizujListener<T> implements TableModelListener {
		Service<T> service;
		Fetchable<T> fetch;
		
		public AktualizujListener(Service<T> service, Fetchable<T> fetch) {
			this.service = service;
			this.fetch = fetch;
		}
		
        @Override
        public void tableChanged(TableModelEvent arg0) {
            if (arg0.getType() == TableModelEvent.UPDATE) {
                int id = arg0.getFirstRow();
                T t = fetch.getList().get(id);
                service.dodaj(t);
            }
        }

    }
	
	class FormListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(OPCJA == 0) {
				trenerForm.setVisible(true);
			} else if(OPCJA == 1) {
				stadionForm.setVisible(true);
			} else if(OPCJA == 2) {
				klubForm.setVisible(true);
			} else if(OPCJA == 3) {
				pilkarzForm.setVisible(true);
			}
		}
		
	}
	
	class UsunListener<T> implements ActionListener {
		Service<T> service;
		Fetchable<T> fetch;

		
		public UsunListener(Service<T> service, Fetchable<T> fetch) {
			this.service = service;
			this.fetch = fetch;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			int[] array = view.getJtable().getSelectedRows();
			
            for (int i = 0; i < array.length; i++) {
            	T t = fetch.getList().get(array[i] - i);
                fetch.usun(array[i] - i);
                service.usun(t);
            }
			
		}
	}
	
	class DodajListener<T> implements ActionListener {
		
		Objectable<T> objectable;
		Service<T> service;
		Fetchable<T> fetch;
		JDialog jd;
		
		public DodajListener(Objectable<T> objectable, 
				Service<T> service, 
				Fetchable<T> fetch, JDialog jd) {
			this.objectable = objectable;
			this.service = service;
			this.fetch = fetch;
			this.jd = jd;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			T t = objectable.getObject();
			service.dodaj(t);
			fetch.addRow(t);
			jd.setVisible(false);
		}
	}
	
	class ComboBoxColumn<T> {
		Service<T> service;
		
		public ComboBoxColumn(int columnNr, Service<T> service) {
			this.service = service;
			setUpColumn(columnNr);
		}
		
		public void setUpColumn(int columnNr) {
			TableColumn column = view.getJtable().getColumnModel().getColumn(columnNr);
			JComboBox<T> comboBox = new JComboBox<T>();
			List<T> lista = service.getAll();
					for(T t : lista) {
						comboBox.addItem(t);
					}

					column.setCellEditor(new DefaultCellEditor(comboBox));
					
					//Set up tool tips for the sport cells.
					DefaultTableCellRenderer renderer =
					new DefaultTableCellRenderer();
					renderer.setToolTipText("Click for combo box");
					column.setCellRenderer(renderer);
		}
	}
	
	
	
}


