package view.models;

import java.util.List;

import javax.swing.table.AbstractTableModel;

abstract public class GenericTableModel<T> extends AbstractTableModel{
	private static final long serialVersionUID = -9056625553908580890L;
	
	public String[] COLUMNS;
	
	public GenericTableModel() {}
	
	public GenericTableModel(String[] columns) {
		COLUMNS = columns;
	}
	
	protected List<T> objects = null;

	@Override
	public int getColumnCount() {
		return COLUMNS.length;
	}

	@Override
	public int getRowCount() {
		if(objects == null)
			return 0;
		return objects.size();
	}

	@Override
	abstract public Object getValueAt(int arg0, int arg1);
		
		
	
	
	@Override
	public String getColumnName(int column) {
		return COLUMNS[column];
	}

	public List<T> getList() {
		return objects;
	}

	public void setObjects(List<T> ts) {
		this.objects = ts;
	}
	
	
	public void addRow(T t) {
		objects.add(t);
		fireTableRowsInserted(objects.size() - 1, objects.size() - 1);
	}
	
	@Override
	abstract public void setValueAt(Object Value, int rowIndex, int columnIndex);
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	public void usun(int row) {
		objects.remove(row);
        fireTableRowsDeleted(row, row);
	}
}
