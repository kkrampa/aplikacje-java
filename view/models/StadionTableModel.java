package view.models;

import view.Fetchable;

import model.Stadion;

public class StadionTableModel extends GenericTableModel<Stadion> implements Fetchable<Stadion>{

	private static final long serialVersionUID = -9056625553908580890L;
	
	public static final String[] COLUMNS = {"nazwa", "ulica", "nr", "miejscowosc", "kod"};

	public StadionTableModel() {
		super.COLUMNS = COLUMNS;
	}
	
	@Override
	public Object getValueAt(int arg0, int arg1) {
		Stadion stadion = objects.get(arg0);
		switch(arg1) {
		case -1:
			return stadion.getId();
		case 0:
			return stadion.getNazwa();
		case 1:
			return stadion.getUlica();
		case 2:
			return stadion.getNr();
		case 3:
			return stadion.getMiejscowosc();
		case 4:
			return stadion.getKod();
		default:
			return 0;
		}
	}
	
	@Override
	public void setValueAt(Object Value, int rowIndex, int columnIndex) {
		
		Stadion stadion = objects.get(rowIndex);
		switch(columnIndex) {
		case -1:
			stadion.setId((Long)Value);
			break;
		case 0:
			stadion.setNazwa((String)Value);
			break;
		case 1:
			stadion.setUlica((String)Value);
		case 2:
			stadion.setNr((Integer)Value);
		case 3:
			stadion.setMiejscowosc((String)Value);
		case 4:
			stadion.setKod((String)Value);
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}

}
