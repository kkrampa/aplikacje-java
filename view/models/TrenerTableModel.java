package view.models;

import view.Fetchable;

import model.Trener;

public class TrenerTableModel extends GenericTableModel<Trener> implements Fetchable<Trener>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5792620975684179901L;
	
	public static final String[] COLUMNS = {"imie", "nazwisko"};
	
	public TrenerTableModel() {
		super.COLUMNS = TrenerTableModel.COLUMNS;
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		Trener trener = objects.get(arg0);
		switch(arg1) {
		case -1:
			return trener.getId();
		case 0:
			return trener.getImie();
		case 1:
			return trener.getNazwisko();
		default:
			return 0;
		}
	}

	@Override
	public void setValueAt(Object Value, int rowIndex, int columnIndex) {
		
		Trener trener = objects.get(rowIndex);
		switch(columnIndex) {
		case -1:
			trener.setId((Long)Value);
			break;
		case 0:
			trener.setImie((String)Value);
			break;
		case 1:
			trener.setNazwisko((String)Value);
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}

}
