package view.models;

import view.Fetchable;

import model.Klub;
import model.Pilkarz;


public class PilkarzTableModel extends GenericTableModel<Pilkarz> implements Fetchable<Pilkarz>{

	private static final long serialVersionUID = -9056625553908580890L;
	
	public static final String[] COLUMNS = {"imie", "nazwisko", "klub", "foto"};
	
	public PilkarzTableModel() {
		super.COLUMNS = COLUMNS;
	}
	
	@Override
	public Object getValueAt(int arg0, int arg1) {
		Pilkarz pilkarz = objects.get(arg0);
		switch(arg1) {
		case -1:
			return pilkarz.getId();
		case 0:
			return pilkarz.getImie();
		case 1:
			return pilkarz.getNazwisko();
		case 2:
			return pilkarz.getKlub();
		case 3: 
			return pilkarz.getFoto();
		default:
			return 0;
		}
	}
	
	@Override
	public void setValueAt(Object Value, int rowIndex, int columnIndex) {
		
		Pilkarz pilkarz = objects.get(rowIndex);
		switch(columnIndex) {
		case -1:
			pilkarz.setId((Long)Value);
			break;
		case 0:
			pilkarz.setImie((String)Value);
			break;
		case 1:
			pilkarz.setNazwisko((String)Value);
			break;
		case 2:
			pilkarz.setKlub((Klub)Value);
			break;
		case 3:
			//pilkarz.setTrener((Trener)Value);
			pilkarz.setFoto((byte[])Value);
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}
}
