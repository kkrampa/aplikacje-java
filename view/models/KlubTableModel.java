package view.models;

import view.Fetchable;

import model.Klub;
import model.Stadion;
import model.Trener;

public class KlubTableModel extends GenericTableModel<Klub> implements Fetchable<Klub>{

	private static final long serialVersionUID = -9056625553908580890L;
	
	public static String[] COLUMNS = {"nazwa", "miejscowosc", "stadion", "trener"};
	
	public KlubTableModel() {
		super.COLUMNS = COLUMNS;
	}
	
	@Override
	public Object getValueAt(int arg0, int arg1) {
		Klub klub = objects.get(arg0);
		switch(arg1) {
		case -1:
			return klub.getId();
		case 0:
			return klub.getNazwa();
		case 1:
			return klub.getMiejscowosc();
		case 2:
			return klub.getStadion().toString();
		case 3: 
			return klub.getTrener().toString();
		default:
			return 0;
		}
	}
	
	@Override
	public void setValueAt(Object Value, int rowIndex, int columnIndex) {
		
		Klub klub = objects.get(rowIndex);
		switch(columnIndex) {
		case -1:
			klub.setId((Long)Value);
			break;
		case 0:
			klub.setNazwa((String)Value);
			break;
		case 1:
			klub.setMiejscowosc((String)Value);
			break;
		case 2:
			klub.setStadion((Stadion)Value);
			break;
		case 3:
			klub.setTrener((Trener)Value);
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}

	
	


}
