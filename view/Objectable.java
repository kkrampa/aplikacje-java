package view;

public interface Objectable<T>{
	T getObject();
}
