package view.forms;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public abstract class Form<T> extends JDialog {
	
	private static final long serialVersionUID = 3360159112820818962L;
	protected JButton zapiszButton;
    protected String[] fields = null;

	protected Map<String, JComponent> textFields = new HashMap<String, JComponent>();

    
    public Form(String[] fields) {
    	this.fields = fields;
		init();
	}
    
    public Form() {}
    	
	public void init() {
		this.setModal(true);
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        GridLayout gridLayout = new GridLayout(fields.length + 1, 2);
        gridLayout.setVgap(20);
        this.getContentPane().setLayout(gridLayout);
        this.setLocationRelativeTo(null);
        for(int i = 0; i < fields.length; i++) {
        	JLabel lblNewLabel = new JLabel(fields[i] + ": ");
            this.getContentPane().add(lblNewLabel);
            JTextField textField = new JTextField();
            textField.setHorizontalAlignment(SwingConstants.LEFT);
            textField.setSize(10, 10);
            this.getContentPane().add(textField);
            textField.setColumns(10);
            textFields.put(fields[i], textField);
        }
        zapiszButton = new JButton("Zapisz");
        this.getContentPane().add(zapiszButton);
	}
	
	public void addZapiszListener(ActionListener l) {
		zapiszButton.addActionListener(l);
	}
	
	public void removeZapiszListener(ActionListener l) {
		zapiszButton.removeActionListener(l);
	}

}
