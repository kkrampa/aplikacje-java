package view.forms;

import javax.swing.JTextField;

import view.Objectable;
import view.models.TrenerTableModel;

import model.Trener;

public class TrenerForm extends Form<Trener> implements Objectable<Trener>{

	public TrenerForm() {
		super(TrenerTableModel.COLUMNS);
	}

	private static final long serialVersionUID = -3616695363372287806L;
		
	public Trener getObject() {
		Trener trener = new Trener();
		trener.setImie(((JTextField)textFields.get("imie")).getText());
		trener.setNazwisko(((JTextField)textFields.get("nazwisko")).getText());
		return trener;
	}
	
}
