package view.forms;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import view.Objectable;
import view.models.PilkarzTableModel;

import model.Klub;
import model.Pilkarz;
import model.Stadion;

public class PilkarzForm extends Form<Pilkarz> implements Objectable<Pilkarz>{
	
	private List<Klub> kluby;
	private JFileChooser fc;
	private byte[] bFile;
	private JButton openDialog = new JButton("Dodaj zdjecie");
	private JLabel foto = new JLabel();
	
	public PilkarzForm(List<Klub> kluby) {
		fc = new JFileChooser();
		this.kluby = kluby;
		fields = PilkarzTableModel.COLUMNS;
		init();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public Pilkarz getObject() {
		Pilkarz pilkarz = new Pilkarz();
		pilkarz.setImie(((JTextField)textFields.get("imie")).getText());
		pilkarz.setNazwisko(((JTextField)textFields.get("nazwisko")).getText());
		JComboBox<Stadion> kluby = (JComboBox<Stadion>)textFields.get("klub");
		pilkarz.setKlub((Klub)kluby.getSelectedItem());
		pilkarz.setFoto(bFile);
		return pilkarz;
	}
	
	public void init() {
		this.setModal(true);
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        GridLayout gridLayout = new GridLayout(6, 2);
        gridLayout.setVgap(20);
        this.getContentPane().setLayout(gridLayout);
        this.setLocationRelativeTo(null);
        for(int i = 0; i < 2; i++) {
        	JLabel lblNewLabel = new JLabel(fields[i] + ": ");
            this.getContentPane().add(lblNewLabel);
            JTextField textField = new JTextField();
            textField.setHorizontalAlignment(SwingConstants.LEFT);
            textField.setSize(10, 10);
            this.getContentPane().add(textField);
            textField.setColumns(10);
            textFields.put(fields[i], textField);
        }
        
        JLabel lblNewLabel = new JLabel("klub: ");
        this.getContentPane().add(lblNewLabel);
        
        JComboBox<Klub> kluby = new JComboBox<Klub>(); 
        for(Klub klub : this.kluby) {
        	kluby.addItem(klub);
        }
        textFields.put("klub", kluby);
        this.getContentPane().add(kluby);
        
        
        zapiszButton = new JButton("Zapisz");
        this.getContentPane().add(zapiszButton);
        
        openDialog.addActionListener(new OpenDialogListener());
        this.getContentPane().add(openDialog);
        this.getContentPane().add(foto);

	}
	
	class OpenDialogListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int returnVal = fc.showOpenDialog(PilkarzForm.this);
			 
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
            	FileInputStream fileInputStream = null;
                bFile = new byte[(int) file.length()];

            	try {
					fileInputStream = new FileInputStream(file);
					fileInputStream.read(bFile);
	        	    fileInputStream.close();
	        	    foto.setIcon(new ImageIcon(bFile));
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
        	    
            		
            }
		}
		
	}

}
