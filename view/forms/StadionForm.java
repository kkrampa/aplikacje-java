package view.forms;

import javax.swing.JTextField;

import view.Objectable;
import view.models.StadionTableModel;

import model.Stadion;

public class StadionForm extends Form<Stadion> implements Objectable<Stadion>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4218269169663202279L;

	public StadionForm() {
		super(StadionTableModel.COLUMNS);
	}

	@Override
	public Stadion getObject() {
		Stadion stadion = new Stadion();
		stadion.setNazwa(((JTextField)textFields.get("nazwa")).getText());
		stadion.setMiejscowosc(((JTextField)textFields.get("miejscowosc")).getText());
		stadion.setUlica(((JTextField)textFields.get("ulica")).getText());
		stadion.setKod(((JTextField)textFields.get("kod")).getText());
		stadion.setNr(Integer.parseInt(((JTextField)textFields.get("nr")).getText()));
		return stadion;
	}

}
