package view.forms;

import java.awt.GridLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import view.Objectable;
import view.models.KlubTableModel;

import model.Klub;
import model.Stadion;
import model.Trener;

public class KlubForm extends Form<Klub> implements Objectable<Klub>{
	
	private List<Stadion> stadiony;
	private List<Trener> trenerzy;
	
	public KlubForm(List<Stadion> stadiony, List<Trener> trenerzy) {
		this.stadiony = stadiony;
		this.trenerzy = trenerzy;
		fields = KlubTableModel.COLUMNS;
		init();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public Klub getObject() {
		Klub klub = new Klub();
		klub.setNazwa(((JTextField)textFields.get("nazwa")).getText());
		klub.setMiejscowosc(((JTextField)textFields.get("miejscowosc")).getText());
		JComboBox<Stadion> stadiony = (JComboBox<Stadion>)textFields.get("stadion");
		klub.setStadion((Stadion)stadiony.getSelectedItem());
		
		JComboBox<Stadion> trenerzy = (JComboBox<Stadion>)textFields.get("trener");
		klub.setTrener((Trener)trenerzy.getSelectedItem());
		return klub;
	}
	
	public void init() {
		this.setModal(true);
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        GridLayout gridLayout = new GridLayout(5, 2);
        gridLayout.setVgap(20);
        this.getContentPane().setLayout(gridLayout);
        this.setLocationRelativeTo(null);
        for(int i = 0; i < 2; i++) {
        	JLabel lblNewLabel = new JLabel(fields[i] + ": ");
            this.getContentPane().add(lblNewLabel);
            JTextField textField = new JTextField();
            textField.setHorizontalAlignment(SwingConstants.LEFT);
            textField.setSize(10, 10);
            this.getContentPane().add(textField);
            textField.setColumns(10);
            textFields.put(fields[i], textField);
        }
        
        JLabel lblNewLabel = new JLabel("stadion: ");
        this.getContentPane().add(lblNewLabel);
        
        JComboBox<Stadion> stadiony = new JComboBox<Stadion>(); 
        for(Stadion stadion : this.stadiony) {
        	stadiony.addItem(stadion);
        }
        textFields.put("stadion", stadiony);
        this.getContentPane().add(stadiony);
        
        JLabel lblNewLabel2 = new JLabel("trener: ");
        this.getContentPane().add(lblNewLabel2);
        JComboBox<Trener> trenerzy = new JComboBox<Trener>();
        for(Trener trener: this.trenerzy) {
        	trenerzy.addItem(trener);
        }
        textFields.put("trener", trenerzy);
        this.getContentPane().add(trenerzy);
        zapiszButton = new JButton("Zapisz");
        this.getContentPane().add(zapiszButton);


	}
	

}
