package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;


public class View extends JFrame{

	private static final long serialVersionUID = 750771599089231076L;
	private JTable jtable;
	private JButton trener;
	private JButton stadion;
	private JButton pilkarz;
	private JButton klub;
	protected JButton zapiszButton;
    protected JButton usunButton;
    protected JButton dodajButton;
	
	public View() {
		init();
	}
	
	
	private void init() {
		JPanel panel = new JPanel(new FlowLayout());
		trener = new JButton("Trener");
		stadion = new JButton("Stadion");
		klub = new JButton("Klub");
		pilkarz = new JButton("Pilkarz");
		panel.add(trener);
		panel.add(stadion);
		panel.add(klub);
		panel.add(pilkarz);
		getContentPane().add(panel, BorderLayout.NORTH);
		jtable = new JTable();
		
		setTitle("Projekt\r\n");

        setSize(800, 600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        scrollPane.setViewportView(jtable);
        setVisible(true);
        dodajButton = new JButton("Dodaj");
        setUsunButton(new JButton("Usuń"));
        JPanel jp = new JPanel(new FlowLayout());
        getContentPane().add(jp, BorderLayout.SOUTH);
        jp.add(dodajButton);
        jp.add(getUsunButton()); 
        pack();

	}

	public JTable getJtable() {
		return jtable;
	}

	public void setJtable(JTable jtable) {
		this.jtable = jtable;
	}
	
	public void addTrenerListener(ActionListener l) {
		trener.addActionListener(l);
	}
	
	public void addStadionListener(ActionListener l) {
		stadion.addActionListener(l);
	}
	
	public void addklubListener(ActionListener l) {
		klub.addActionListener(l);
	}
	
	public void adddodajListener(ActionListener l) {
		dodajButton.addActionListener(l);
	}
	
	public void addUsunListener(ActionListener l) {
		usunButton.addActionListener(l);
	}
	
	public void removeUsunListener(ActionListener l) {
		usunButton.removeActionListener(l);
	}


	public void addPilkarzListener(ActionListener l) {
		pilkarz.addActionListener(l);
	}
		
    
	public void setZapiszButton(JButton zapiszButton) {
		this.zapiszButton = zapiszButton;
	}
	
	public JButton getZapiszButton() {
		return zapiszButton;
	}
	
	public void setUsunButton(JButton usunButton) {
		this.usunButton = usunButton;
	}
	
	public JButton getUsunButton() {
		return usunButton;
	}

	public void addDodajListener(ActionListener a) {
		dodajButton.addActionListener(a);
	}
}
