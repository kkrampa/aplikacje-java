package view;

import java.util.List;

public interface Fetchable<T> {
	List<T> getList();
	void addRow(T t);
	void usun(int row);
}
